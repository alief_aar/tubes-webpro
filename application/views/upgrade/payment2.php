<?php
defined('BASEPATH') OR exit('No script allowed here');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title> Wix Premium Upgrade Plans </title>
		<link rel="icon" type="image/png" href="http://localhost/tubes/assets/foto/title.png" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"><style>
			.gambar{
				width : 7%;
			}
			.background{
				background-color: #e8f0ff;
				width : 100%;
				height: 10%;
			}
			.posisiCard{
				margin-top: -20%;
				display: block;
				margin-left: 2.5%;
			}
			.btn{
				border-radius: 25px;
				color : #76D7C4;
				background-color: white;
				border-color: #76D7C4;
				border-style: solid;
				padding-left: 35px;
				padding-right: 35px;
			}
			.btn:hover{
				color: white;
				background-color: #76D7C4;
			}
			.card{
				border-style: solid;
				border-color: grey;
				margin-bottom: 10%;
			}
			.imgCard{
				margin-left: -12%;
				margin-top : -12%;
				width       : 124%;
			}
			#aturan{
				margin-top: -9%;
				margin-left: 6%;
				font-size:12px;
			}
			form{
				width: 1000px;
				max-height: 430px;
				margin-left : 10%;
				margin-top: 10%;
				padding-top: 5%;
				padding-left: 1%;
				padding-right: 1%;
				padding-bottom : 30%;
				margin-bottom: 10%;
				background:white;
				border-color:black;
				border-radius: 10px;
				display: inline-block;
				/*
				background:white;
				display: flex;
				flex-direction: column;
				*/
			}
			.content {
			  padding: 0 18px;
			  max-height: 0;
			  overflow: hidden;
			  transition: max-height 0.2s ease-out;
			  background-color: transparent;
			}
			.hrrr{ 
				border-left: 1px solid #e8f0ff;
				height: 65vh;
				width: 1px;     
				display: block;
				margin-top: -40%;
				margin-left: 55%;
			}
			input[type='radio']{
				margin-left:1%;
			}
			label{
				margin-left:5%;
			}
			input[type='radio']:after {
				width: 15px;
				height: 15px;
				border-radius: 15px;
				top: -2px;
				left: -1px;
				position: relative;
				background-color: white;
				content: '';
				display: inline-block;
				visibility: visible;
				border: 0.5px solid black;
			}
			input[type='radio']:checked:after {
				width: 15px;
				height: 15px;
				border-radius: 15px;
				top: -2px;
				left: -1px;
				position: relative;
				background-color: #84b1f9;
				content: '';
				display: inline-block;
				visibility: visible;
				border:  2px solid white;
			}
			.submitButton{
				background-color: transparent;
				border: none;
				cursor: pointer;
				outline: none;
				margin-left :4%;
				padding-top: -20%;
				text-decoration: none;
			}
			.submitButton2{
				background-color: #8cb8ff;
				border: none;
				border-radius: 20px;
				cursor: pointer;
				outline: none;
				margin-left: 35%;
				margin-top: -2%;
				color: white;
				padding: 5px 15px;
			}
			.paymentMethod{
				margin-left: 65%;
				margin-top: -80%;	
			}
			.sslPayment{
				margin-left: 65%;
				margin-top: 0%;
			}
			.guaranteePayment{
				margin-left:65%;
				margin-top: 0%;
			}
			.rrrh{
				width: 300px;
				margin-left: 63%;
				margin-top: -35%;
			}
		</style>
	</head>
	<body>
		<!-- as heading componen -->
		<nav class="navbar navbar-white bg-white" >
			<span class="navbar-brand "><img src="<?php echo base_url('assets/foto/judul.png');?>" class="gambar" /></span>
		</nav>
		<div class="background">
			<div class="container">
				<form method="post" action="">
					<div class="col-md-6">
						<p style="font-size:20px;margin-top:-8%;padding-bottom:5%;"> Select your Unlimited Plan subscription </p> 
							<div class="form-group">
								  <img src="<?php echo base_url('assets/foto/save.png');?>"/>
								  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" >
								  <label class="form-check-label" style="font-size:13px;font-weight:bold" for="inlineRadio1">2 Year Subscription: US$<?php echo $paket; ?> x 24 Months</label>
								  <p style="font-size:10px;"><img src="<?php echo base_url('assets/foto/check.png');?>" style="padding-left:28%"/> Free Domain for 1 Year </p>
								  <p style="font-size:10px;"><img src="<?php echo base_url('assets/foto/check.png');?>" style="padding-left:28%"/> 2 Free Premium Apps - $108 Value </p>
							</div>
							<div class="form-group">
							      <img src="<?php echo base_url('assets/foto/save45.png');?>"/>
								  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
								  <label class="form-check-label" style="font-size:13px;font-weight:bold" for="inlineRadio2">Yearly Subscription: US$<?php echo $paket; ?> x 12 Months</label>
								  <p style="font-size:10px;"><img src="<?php echo base_url('assets/foto/check.png');?>" style="padding-left:28%"/> Free Domain for 1 Year </p>
								  <p style="font-size:10px;"><img src="<?php echo base_url('assets/foto/check.png');?>" style="padding-left:28%"/> 2 Free Premium Apps - $108 Value </p>
							</div>
							<div class="form-group" style="margin-left:24%">
								  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
								  <label class="form-check-label" style="font-size:13px;padding-left:2%;font-weight:bold" for="inlineRadio3">Monthly Subscription: US$<?php echo $paket; ?> month to month</label>
								  <p style="font-size:10px;padding-left:7%;" class="form-check-label" for="inlineRadio3"> Note : The monthly subscription does not include a free domain voucher and premium apps </p>
							</div>
					</div>
					<div class="col">
						<a href="<?php echo base_url('website/upgrade')?>" class="submitButton d-inline-block" style="margin-top: 1%;">Back</a>
						<button type="submit" class="submitButton2 d-inline-block" onclick="location.href='<?php echo base_url();?>website/payment2'"> Select </button>
					</div>
					<hr class="hrrr">
					<img src="<?php echo base_url('assets/foto/paymentMethod.png')?>" class="paymentMethod"/>
					<hr class="rrrh">
					<img src="<?php echo base_url('assets/foto/sslPayment.png')?>" class="sslPayment"/>
					<img src="<?php echo base_url('assets/foto/guaranteePayment.png')?>" class="guaranteePayment"/>
				</form>
			  </div>
			</div>
		</div>
	
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>

