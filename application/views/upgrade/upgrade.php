<?php
defined('BASEPATH') OR exit('No script allowed here');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title> Wix Premium Upgrade Plans </title>
		<link rel="icon" type="image/png" href="http://localhost/tubes/assets/foto/title.png" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"><style>
			.gambar{
				width : 7%;
			}
			.background{
				background-color: #CACFD2;
				width : 100%;
				height: 10%;
			}
			.posisiCard{
				margin-top: -20%;
				display: block;
				margin-left: 2.5%;
			}
			.btn{
				border-radius: 25px;
				color : #76D7C4;
				background-color: white;
				border-color: #76D7C4;
				border-style: solid;
				padding-left: 35px;
				padding-right: 35px;
			}
			.btn:hover{
				color: white;
				background-color: #76D7C4;
			}
			.card{
				border-style: solid;
				border-color: grey;
				margin-bottom: 10%;
			}
			.imgCard{
				margin-left: -12%;
				margin-top : -12%;
				width       : 124%;
			}
			#aturan{
				margin-top: -9%;
				margin-left: 6%;
				font-size:12px;
			}
			i {
			  border: solid #76D7C4;
			  border-width: 0 3px 3px 0;
			  display: inline-block;
			  padding: 3px;
			}

			.right {
			  transform: rotate(-45deg);
			  -webkit-transform: rotate(-45deg);
			}
			.down {
			  transform: rotate(45deg);
			  -webkit-transform: rotate(45deg);
			}
			.active,.submitButton{
				background-color: transparent;
				border: none;
				cursor: pointer;
				outline: none;
			}
			.submitButton:focus{
				outline: 0;
			}
			.content {
			  padding: 0 18px;
			  max-height: 0;
			  overflow: hidden;
			  transition: max-height 0.2s ease-out;
			  background-color: transparent;
			}
			#myDIV{
				transition: 0.5s;
			}
			#myDIV2{
				transition: 0.5s;
			}
			#myDIV3{
				transition: 0.5s;
			}
			#myDIV4{
				transition: 0.5s;
			}
			#myDIV5{
				transition: 0.5s;
			}
			#myDIV6{
				transition: 0.5s;
			}
			#myDIV7{
				transition: 0.5s;
			}
			#myDIV8{
				transition: 0.5s;
			}
			#myDIV9{
				transition: 0.5s;
			}
			#myDIV10{
				transition: 0.5s;
			}
			#myDIV11{
				transition: 0.5s;
			}
			#myDIV12{
				transition: 0.5s;
			}
		</style>
	</head>
	<body>
		<!-- as heading componen -->
		<nav class="navbar navbar-white bg-white" >
			<span class="navbar-brand "><img src="<?php echo base_url('assets/foto/judul.png');?>" class="gambar" /></span>
		</nav>
		<div class="background">
			<div class="d-flex justify-content-center">
				<p style="margin-top : 5%;font-size:30px;"> - Limited Time Only - </p>
			</div>
			<div class="d-flex justify-content-center">
				<h3 style="font-size:40px;"> SAVE 50% on Yearly Unlimited & eCommerce Plans </h3>
			</div>
			<div class="d-flex justify-content-center">
				<p style="margin-top : 1%;margin-bottom: 30%;font-size:20px;"> 14 Day Money Back Guanrantee on All Wix Premium Plans </p>
			</div>
		</div>
		<?php foreach ($user as $apa) {?>
		<div class="card posisiCard d-inline-block" style="width: 13rem;margin-left:6%;">
			  <div class="card-body">
				<div class="text-center">
					<h5 class="card-title" style="margin-top:11%">VIP</h5>
					<p class="card-text">Everything and More</p>
					<p class="card-text d-inline" style="font-size:30px">US$</p>
					<p class="card-text d-inline" style="font-size:30px;font-weight:bold">24</p>
					<sup class="card-text d-inline">50</sup>
					<sub class="card-text d-inline">/month</sub>
					<a href="<?php echo base_url('Website/upgrade/'.$apa->username.'/vip')?>" class="btn btn-primary" style="margin-top: 15%;" value="24.5">Select</a>
				</div>
				<div class="text-left" style="font-size:12px">
					<p class="card-text" style="font-weight:bold;text-align:left;margin-top: 13%"> Unlimited Bandwidth </p>
					<p class="card-text" style="font-weight:bold;text-align:left"> 20GB Storage </p>
					<p class="card-text"> Connect Your Domain </p>
					<p class="card-text"> Free Domain </p>
					<p class="card-text"> Remove Wix Ads </p>
					<p class="card-text"> Customize Favicon </p>
					<p class="card-text"> Form Builder App - $48 Value </p>
					<p class="card-text"> Site Booster App - $60 Value </p>
					<p class="card-text"> Online Store </p>
					<p class="card-text"> 10 Email Campaigns/month </p>
					<p class="card-text"> Priority Response </p>
				</div>
			  </div>
		</div>
		<div class="card posisiCard d-inline-block" style="width: 13rem;margin-left:2%;">
			  <div class="card-body">
				<img class="card-img-top imgCard" src="<?php echo base_url('assets/foto/card_header.png')?>" alt="Card image cap">
				<div class="text-center">
					<h5 class="card-title" style="margin-top:15%;">eCommerce</h5>
					<p class="card-text">Best for Small Business</p>
					<p class="card-text d-inline" style="font-size:30px">US$</p>
					<p class="card-text d-inline" style="font-size:30px;font-weight:bold">8</p>
					<sup class="card-text d-inline">25</sup>
					<sub class="card-text d-inline">/month</sub>
					<p class="card-text" style="text-decoration: line-through"> US$ 16.50 </p>
					<a href="<?php echo base_url('website/upgrade/'.$apa->username.'/ecommerce')?>" class="btn btn-primary" style="margin-top: 3%;" value="8.25">Select</a>
				</div>
				<div class="text-left" style="font-size:12px">
					<p class="card-text" style="font-weight:bold;text-align:left;margin-top: 13%"> Unlimited Bandwidth </p>
					<p class="card-text" style="font-weight:bold;text-align:left"> 20GB Storage </p>
					<p class="card-text"> Connect Your Domain </p>
					<p class="card-text"> Free Domain </p>
					<p class="card-text"> Remove Wix Ads </p>
					<p class="card-text"> Customize Favicon </p>
					<p class="card-text"> Form Builder App - $48 Value </p>
					<p class="card-text"> Site Booster App - $60 Value </p>
					<p class="card-text"> Online Store </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
				</div>
			  </div>
		</div>
		<div class="card posisiCard d-inline-block" style="width: 13rem;margin-left:2%;">
			  <div class="card-body">
			  <img class="card-img-top imgCard" src="<?php echo base_url('assets/foto/card_header.png')?>" alt="Card image cap">
				<div class="text-center">
					<h5 class="card-title" >Unlimited</h5>
					<p class="card-text">Entrepreneurs & Freelancers</p>
					<p class="card-text d-inline" style="font-size:30px">US$</p>
					<p class="card-text d-inline" style="font-size:30px;font-weight:bold">6</p>
					<sup class="card-text d-inline">25</sup>
					<sub class="card-text d-inline">/month</sub>
					<p class="card-text" style="text-decoration: line-through"> US$ 12.50 </p>
					<a href="<?php echo base_url('website/upgrade/'.$apa->username.'/unlimited')?>" class="btn btn-primary" style="margin-top: 3%;" value="6.25">Select</a>
				</div>
				<div class="text-left" style="font-size:12px">
					<p class="card-text" style="font-weight:bold;text-align:left;margin-top: 13%"> Unlimited Bandwidth </p>
					<p class="card-text" style="font-weight:bold;text-align:left"> 10GB Storage </p>
					<p class="card-text"> Connect Your Domain </p>
					<p class="card-text"> Free Domain </p>
					<p class="card-text"> Remove Wix Ads </p>
					<p class="card-text"> Customize Favicon </p>
					<p class="card-text"> Form Builder App - $48 Value </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
				</div>
			  </div>
		</div>
		<div class="card posisiCard d-inline-block" style="width: 13rem;margin-left:2%;">
			  <div class="card-body">
				<div class="text-center">
					<h5 class="card-title" >Combo</h5>
					<p class="card-text">For Personal Use</p>
					<p class="card-text d-inline" style="font-size:30px">US$</p>
					<p class="card-text d-inline" style="font-size:30px;font-weight:bold">6</p>
					<sup class="card-text d-inline">25</sup>
					<sub class="card-text d-inline">/month</sub>
					<p class="card-text" style="text-decoration: line-through"> US$ 12.50 </p>
					<a href="<?php echo base_url('website/upgrade/'.$apa->username.'/combo')?>" class="btn btn-primary" style="margin-top: 3%;" value="6.25">Select</a>
				</div>
				<div class="text-left" style="font-size:12px">
					<p class="card-text" style="font-weight:bold;text-align:left;margin-top: 13%"> Unlimited Bandwidth </p>
					<p class="card-text" style="font-weight:bold;text-align:left"> 10GB Storage </p>
					<p class="card-text"> Connect Your Domain </p>
					<p class="card-text"> Free Domain </p>
					<p class="card-text"> Remove Wix Ads </p>
					<p class="card-text"> Customize Favicon </p>
					<p class="card-text"> Form Builder App - $48 Value </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
				</div>
			  </div>
		</div>
		<div class="card posisiCard d-inline-block" style="width: 13rem;margin-left:2%;">
			  <div class="card-body">
				<div class="text-center">
					<h5 class="card-title" style="margin-top:11%">Connect Domain</h5>
					<p class="card-text">Most Basic</p>
					<p class="card-text d-inline" style="font-size:30px">US$</p>
					<p class="card-text d-inline" style="font-size:30px;font-weight:bold">4</p>
					<sup class="card-text d-inline">50</sup>
					<sub class="card-text d-inline">/month</sub>
					<a href="<?php echo base_url('website/upgrade/'.$apa->username.'/connect')?>" class="btn btn-primary" style="margin-top: 15%;" value="4.5">Select</a>
				</div>
				<div class="text-left" style="font-size:12px">
					<p class="card-text"  style="font-weight:bold;text-align:left;margin-top: 13%"> 1GB Bandwidth </p>
					<p class="card-text" style="text-align:left"> 500MB Storage </p>
					<p class="card-text"> Connect Your Domain </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
					<p class="card-text"> &nbsp </p>
				</div>
			  </div>
		</div>
		<?php } ?>
		<div id="aturan">
			<p> Site Booster, Form Builder and a free domain are not-included in monthly subscriptions </p>
			<p style="margin-top:-1%"> Offer valid until April 11, 2019 (7 am UTC). Promotion only valid for the initial subscription purchased during this sale. </p>
		</div>

		<p style="text-align:center;font-size: 45px;margin-top:5%;"> All Premium Always Include : </p>

		<br>

		<div class="container">
		  <div class="row">
			<div class="col-sm">
			  <img src="<?php echo base_url('assets/foto/free_hosting.png') ?>" alt="FREE Hosting"/>
			</div>
			<div class="col-sm" style="margin-left:5%;">
			  <img src="<?php echo base_url('assets/foto/domain_connection.png') ?>" alt="Domain Connection"/>
			</div>
			<div class="col-sm">
			  <img src="<?php echo base_url('assets/foto/storage.png') ?>" alt="500MB + Storage"/>
			</div>
			<div class="col-sm">
			  <img src="<?php echo base_url('assets/foto/google_analytics.png') ?>" alt="Google Analytics"/>
			</div>
			<div class="col-sm" style="margin-left:2%;">
			  <img src="<?php echo base_url('assets/foto/headphone.png')  ?>"  alt="Premium Supoort"/>
			</div>
			<div class="col-sm">
			  <img src="<?php echo base_url('assets/foto/setup.png') ?>" alt="No Set-Up Free"/>
			</div>
		  </div>

		  <div class="row">
			<div class="col-sm">
			  <p> FREE Hosting </p>
			</div>
			<div class="col-sm">
			  <p> Domain Connection </p>
			</div>
			<div class="col-sm">
			  <p> 500MB + Storage </p>
			</div>
			<div class="col-sm">
			  <p> Google Analytics </p>
			</div>
			<div class="col-sm">
			  <p> Premium Support </p>
			</div>
			<div class="col-sm">
			  <p> No Set-Up Free </p>
			</div>
		  </div>
		</div>

		<div class="background" style="margin-top:5%;">
			<div class="d-flex justify-content-center">
				<p style="margin-top : 5%;font-size:30px;"> Trusted By Millions </p>
			</div>

			<div class="container" >
			  <div class="row">
				<div class="col-sm" style="margin-left:5%;">
				  <img src="<?php echo base_url('assets/foto/SSL.png') ?>" alt="SSL"/>
				</div>
				<div class="col-sm" style="margin-left:-1%;">
				  <img src="<?php echo base_url('assets/foto/guarantee.png') ?>" alt="Guarantee"/>
				</div>
				<div class="col-sm" style="margin-right:5%;">
				  <img src="<?php echo base_url('assets/foto/cc.png') ?>" alt="Credit Card"/>
				</div>
			  </div>
			</div>

			<div class="d-flex justify-content-center">
				<p style="margin-top : 1%;margin-bottom: 5%;font-size:13px;"> Prices do not include VAT, which is determined based on the user's billing country. The final price can be seen on the purchase page, before payment is completed. </p>
			</div>
		</div>

		<div class="container">
		  <div class="row">
			<div class="col" style="margin-top:5%;margin-left:5%">
			  <p style="font-size:35px"> 
			  Some Question <br>
			  You Might Have</p>
			</div>
			<div class="col ">
					 <div class="row" style="margin-top:20%">
						<button type="submit" class="submitButton" onclick="myFunction()"><i class="right" id="myDIV"></i> What's a Premium Plan? </button> 
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">Wix gives you everything you need to create a stunning website for free, including top-grade hosting. You can choose to get even more benefits and features by upgrading to one of our Premium plans.</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction2()"><i class="right" id="myDIV2"></i> What are the benefits of a Premium plan? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">The Premium plan offers many benefits. Highlights include:</p>
						  
						  <p style="margin-left:1%;font-size:12px;font-weight:bold;">The ability to connect your own domain</p>
						  
						  <p style="margin-left:1%;font-size:12px">Your own domain (e.g. www.MyStunningWebsite.com) gives your business credibility and professionalism, and makes it easier for your audience to find you.</p>
						  
						  <p style="margin-left:1%;font-size:12px;font-weight:bold;">Remove Wix Ads</p>	
						  
						  <p style="margin-left:1%;font-size:12px">Free Wix websites display a small banner at the top and bottom of the page promoting Wix. We do not place any other advertisements on your site. Most of our Premium plans let you remove these banners.</p>
						  
						  <p style="margin-left:1%;font-size:12px;font-weight:bold;">Extra bandwidth and storage</p>	
						  
						  <p style="margin-left:1%;font-size:12px">Most of our Premium plans come with extra bandwidth and storage, allowing for higher-res graphics and content on your site.</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction3()"><i class="right" id="myDIV3"></i> What are the benefits of purchasing a Yearly Savings Plan? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">Yearly Plans are very popular because most people need a website for at least a year. Our Yearly Plans offer additional benefits and features, with savings of up to 45%. The Yearly VIP, Yearly eCommerce, Yearly Unlimited and Yearly Combo Plans all include a valuable voucher for a free domain. If you purchase a Yearly Plan, you will automatically be billed on a yearly basis. If you purchase a Monthly Plan, the contract length is one month and you will automatically be billed on a monthly basis. All of our Premium Plans come with a 14 day money back guarantee.</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction4()"><i class="right" id="myDIV4"></i> Is web hosting included with my Wix website?</button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">Yes. Wix provides a complete, reliable hosting solution for your website, with more than 99.9% uptime and uninterrupted service. Web hosting is included for free with or without a Premium plan. When you subscribe to a Premium plan you can also connect your own domain name to your Wix account.</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction5()"><i class="right" id="myDIV5"></i> What does “connect your own domain“ mean? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">If you have a free Wix site, your domain name will be in the following format: username.wixsite.com/sitename. Once you subscribe to any Premium Plan you can connect your own domain name (e.g. www.MyStunningWebsite.com) to your Wix site. 
							Your own domain name is a great way to promote your business and create a professional online impression. Wix offers a free domain with the following Premium Plans: Yearly eCommerce, Yearly Unlimited and Yearly Combo. Click here for instructions on how to connect your domain.</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction6()"><i class="right" id="myDIV6"></i> How do I purchase my own domain? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">You can purchase your own domain with Wix here. If you have purchased a domain elsewhere, you can transfer it to Wix. 
							To connect an existing domain to your Wix site, sign in to your Wix account > hover over Subscriptions > click Domains</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction7()"><i class="right" id="myDIV7"></i> Can I get my own personalized email address? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">Yes. Communicate professionally and promote your business every time you send email with a personalized Mailbox. Wix offers Mailboxes from G Suite by Google Cloud. Every Mailbox comes with an email address using your website domain, 25GB Inbox, 5GB Cloud storage, collaborative Calendar, Docs, Sheets & Slides</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction8()"><i class="right" id="myDIV8"></i> Can I switch my Premium plan from one Wix site to another? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">Yes. To move your Premium Plan to a different Wix site, sign in to your Wix account > hover over Subscriptions > click Premium Plans. Under your site, click Change and select the new site where you'd like to assign your Premium Plan. Click here for more instructions.</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction9()"><i class="right" id="myDIV9"></i> Can I cancel my subscription to a Premium plan? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">Yes. If you cancel your Premium Plan within 14 days, you'll receive a full refund. You will not be charged any fees for canceling before 14 days. If you choose to cancel after this time, you will not be charged for your next billing cycle. To cancel, sign in to your Wix account, hover over the user panel at the top right of the page and click on Billing & Payments. Select the Plan you'd like to cancel and then click Cancel Plan.</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction10()"><i class="right" id="myDIV10"></i> What happens to my Wix site if I cancel my Premium plan? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">If you cancel your Premium Plan, your Wix site will return to the free version, which includes Wix ads and the Wix domain (username.wixsite.com/sitename). We never remove or destroy websites and you will still have full control of your site.</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%">
						<button type="submit" class="submitButton" onclick="myFunction11()"><i class="right" id="myDIV11"></i> Where can I find my billing information? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">You can find your billing information by signing in to your Wix account > hover over the user panel at the top right of the page > click Billing & Payments. For general billing information, click here.</p>
						</div>
					 </div>
					 <div class="row" style="margin-top:5%;margin-bottom:10%;">
						<button type="submit" class="submitButton" onclick="myFunction12()"><i class="right" id="myDIV12"></i> How do I pay for my Premium plan? </button>
						<div class="content">
						  <p style="margin-left:1%;font-size:12px">You can purchase a Premium plan using a credit card and major debit cards. Currently, Wix accepts the following credit cards: Visa, Mastercard, American Express, Discover, JCB and Diners.</p>
						</div>
					 </div>
			</div>
		  </div>
		</div>

		<script>
		var i = 0;
		function myFunction() {
		    if (i%2 == 0){
				document.getElementById("myDIV").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction2() {
		    if (i%2 == 0){
				document.getElementById("myDIV2").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV2").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction3() {
		    if (i%2 == 0){
				document.getElementById("myDIV3").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV3").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction4() {
		    if (i%2 == 0){
				document.getElementById("myDIV4").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV4").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction5() {
		    if (i%2 == 0){
				document.getElementById("myDIV5").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV5").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction6() {
		    if (i%2 == 0){
				document.getElementById("myDIV6").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV6").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction7() {
		    if (i%2 == 0){
				document.getElementById("myDIV7").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV7").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction8() {
		    if (i%2 == 0){
				document.getElementById("myDIV8").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV8").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction9() {
		    if (i%2 == 0){
				document.getElementById("myDIV9").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV9").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction10() {
		    if (i%2 == 0){
				document.getElementById("myDIV10").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV10").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction11() {
		    if (i%2 == 0){
				document.getElementById("myDIV11").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV11").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
		var i = 0;
		function myFunction12() {
		    if (i%2 == 0){
				document.getElementById("myDIV12").style.transform = "rotate(45deg)";
			}
		    else{
				document.getElementById("myDIV12").style.transform = "rotate(-45deg)";
			}
			i++
		}
		</script>

		<script>
			var coll = document.getElementsByClassName("submitButton");
			var i;

			var arrow = document.getElementsByClassName("right");



			for (i = 0; i < coll.length; i++) {
			  coll[i].addEventListener("click", function() {
				this.classList.toggle("active");
				var content = this.nextElementSibling;
				if (content.style.maxHeight){
				  content.style.maxHeight = null;
				} else {
				  content.style.maxHeight = content.scrollHeight + "px";
				} 
			  });
			}
			</script>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>

