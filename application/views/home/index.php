<?php
defined('BASEPATH') OR exit('No script allowed here');
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title> Free Website Builder | Create a Free Website | Wix.com </title>
		<link rel="icon" type="image/png" href="<?php echo base_url('assets/foto/title.png');?>"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<style>
			.collapse{
				display:inline-block;
				margin-left: -25%;
			}
			li:hover{
				background-color: #007bff;
				height: 2px;
			}
			.btn2{
				background-color:white;
				color: #007bff;
				margin : 0 0 0 40%;
			}
			.btn2:hover{
				background-color : #d5d5d5;
				color: #007bff;
				text-decoration: none;
			}
			.2ndContent{
				border: none;
			}
			.boxBagianBawah{
				width: 125.8%;
				height: 100%;
				padding: 100px;
				margin-left: -15%;
				margin-top:5%;
				border: 3px solid #007bff;
				box-sizing: border-box;
				background-color: #007bff;
			}
			.boxPutihKecil{
				border: 1px solid white;
				background-color: white;
				width: 100px;
				height: 2px;
			}
			ol{
				color: white;
				position: static;
			}
			hr{
				width: 150.8%;
				margin-left: -40%;
			}
		</style>
	</head>
	<body>
		<nav class="navbar navbar-expand navbar-light bg-light">
			  <a class="navbar-brand" href="#" style="max-width: 30%;">
				<img src="<?php echo base_url('assets/foto/judul.png');?>" class="img-fluid" style="width: 15%">
			 </a>
			  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
				  <li class="nav-item active">
					<a class="nav-link" href="#"> Feature <span class="sr-only">(current)</span></a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" href="#"> Explore </a>
				  </li>
				  <li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					  Subscription
					</a>
					<div class="dropdown-menu">
					  <a class="dropdown-item" href="<?php echo base_url('website/login')?>">Premium Plans</a>
					  <a class="dropdown-item" href="<?php echo base_url('website/login')?>">Domain</a>
					</div>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('website/login')?>">Templates</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" href="#">Support</a>
				  </li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="<?php echo base_url('website/login')?>">
				  <button class="btn btn-outline-primary my-2 my-sm-0" style="border-radius:25px;padding: 5px 20px 5px 20px" type="submit" name="login">Sign In</button>
				</form>
			  </div>
		</nav>

		<!-- Content -->
		<div class="container">
			<!-- Gambar Awal -->
			<img src="<?php echo base_url('assets/foto/index_atass.jpg')?>" style="width:130.8%;margin:0 -10% 0 -20%;" >
			<h1 style="color:white;position:relative;margin: -53% 0 3% 30%"> 
				The Place to Create 
				<br>
				Professional Websites
			</h1>
			<button class="btn btn-outline-primary my-2 my-sm-0 btn2" style="border-radius:25px;padding: 5px 20px 5px 20px" type="submit" name="login2" onclick="location.href='<?php echo base_url();?>website/login'">Get Started</button>
			
			<!-- Dibawah Gambar (Tulisan) -->
			<div class="row" style="margin-top:50%;margin-bottom: 10%;">
				<div class="col-sm">
				  <h1> The Freedom to Create Anything </h1>
				</div>
				<div class="col-sm">
				  <p> We believe anything is possible with the right website builder. Whether you're about to create a website for the first time or you're a long time pro, we've got you covered. You have two ways to start: Get a free website designed for you with Wix Artificial Design Intelligence or get total design freedom with the Wix Editor. You can always add advanced functionality to your website with Corvid by Wix. It’s time to get your professional website. </p>
				  <p style="text-decoration: underline"> <a href="<?php echo base_url('Website/login')?>" style="background-color: none;">Get Started > </p></a>
				</div>
			</div>

			<!-- Dibawah Tulisan (Gambar) -->
			
			<!-- Gambar 1 -->
			<div class="row" >
				<div class="col-sm">
				  <img src="<?php echo base_url('assets/foto/g1.png')?>"/>
				</div>
				<div class="col-sm" style="font-color:black">
				  <h1> Wix Editor </h1>
				  <p> Total Design Freedom </p>
				  <p> Start with a blank slate or choose from over 500 designer-made templates. With the world’s most innovative drag and drop website builder, you can customize anything you want. Create beautiful websites with video backgrounds, parallax, animation, and more—all without worrying about code. With the Wix Editor, you can design the most stunning websites, all on your own. </p>
				  <p style="text-decoration: underline"> <a href="<?php echo base_url('Website/login')?>" style="background-color: none;">Get Started > </p></a>
				</div>
			</div>

			<!-- Gambar 2 -->
			<div class="row">
				<div class="col-sm">
				  <h1> Corvid </h1>
				  <p> Creation Without Limits </p>
				  <p> Get advanced capabilities with all the design features of the Wix Editor. Build custom web applications and robust websites. It’s serverless, hassle-free coding. You can set up your own database collections, build content rich websites, add custom forms and change site behavior with our APIs. Plus, anything you create is SEO compatible. With our website builder, you can create anything you want. </p>
				  <p style="text-decoration: underline"> <a href="<?php echo base_url('Website/login')?>" style="background-color: none;">Get Started > </p></a>
				</div>
				<div class="col-sm">
					<img src="<?php echo base_url('assets/foto/g2.png')?>"/>
				</div>
			</div>

			<!-- Gambar 3 -->
			<div class="row">
				<div class="col-sm">
				  <img src="<?php echo base_url('assets/foto/g3.png')?>"/>
				</div>
				<div class="col-sm">
				  <h1> Wix Adi </h1>
				  <p> Get Online Fast </p>
				  <p> Get advanced capabilities with all the design features of the Wix Editor. Build custom web applications and robust websites. It’s serverless, hassle-free coding. You can set up your own database collections, build content rich websites, add custom forms and change site behavior with our APIs. Plus, anything you create is SEO compatible. With our website builder, you can create anything you want. </p>
				  <p style="text-decoration: underline"> <a href="<?php echo base_url('Website/login')?>" style="background-color: none;">Get Started > </p></a>	
				</div>
			</div>

			<!-- Judul --> 
			<div class="row" style="justify-content: center;margin-top:5%;margin-bottom:5%;">
				<h1 > Powerful Features <br> for Your Website </h1>
			</div>
			
			<br>

			<!-- Baris 1 -->
			<div class="row">
				<div class="col"  style="margin-left: 15%;margin-top:5%; ">
					<img src="<?php echo base_url('assets/foto/bb.png')?>"/>
					<h6> Beautiful Blog </h6>
					<p> 
						Create a beautiful blog, grow your <br>
						community and share your ideas with <br>
						the world. 
					</p>
				</div>
				<div class="col">

				</div>
				<div class="col" style="margin-right: 15%; ">
					<img src="<?php echo base_url('assets/foto/mg.png')?>"/>
					<h6> Media Galleries </h6>
					<p> 
						Display your high-quality images, videos <br>
						and text in a fully-customizable gallery. 
					</p>
				</div>
			</div>
			
			<br>

			<!-- Baris 2 -->
			<div class="row">
				<div class="col" style="margin-left: 15%;margin-top:10%; ">
					<img src="<?php echo base_url('assets/foto/mf.png')?>"/>
					<h6> Mobile Friendly </h6>
					<p> 
						Look amazing on every screen with a  <br>
						mobile-friendly version of your website.
					</p>
				</div>
				<div class="col">

				</div>
				<div class="col" style="margin-right: 15%; ">
					<img src="<?php echo base_url('assets/foto/os.png')?>"/>
					<h6> Online Store </h6>
					<p> 
						Create a beautiful storefront and manage all  <br>
						your products and orders in one place.
					</p>
				</div>
			</div>

			<br>

			<!-- Baris 3 -->
			<div class="row">
				<div class="col" style="margin-left: 15%;margin-top:5%; ">
					<img src="<?php echo base_url('assets/foto/gg.jpg')?>"/>
					<h6> Custom Domains </h6>
					<p> 
						Get a personalized domain name that  <br>
						matches your brand and business.
					</p>
				</div>
				<div class="col">

				</div>
				<div class="col" style="margin-right: 15%;margin-top:-5% ">
					<img src="<?php echo base_url('assets/foto/seo.jpg')?>"/>
					<h6> Industry-Leading SEO </h6>
					<p> 
						Follow your own personalized SEO plan  <br>
						to get your website found on Google.
					</p>
					<!-- See All Features -->
					<a href="#"><img src="<?php echo base_url('assets/foto/all.jpg')?>" style="cursor: pointer"/></a>
				</div>
			</div>

			<br>
			<br>
			<br>
			<br>

			<!-- Manage -->
			<div class="row">
				<div class="col">
					<div class="row">
						<h1> Manage and Grow Your Business Online </h1>
						<p> No matter who you are—photographer, restaurant owner, musician, hotelier and more, you can manage your website and business all in one place. With Wix, the possibilities are really endless! We offer 200 Apps and services to make it easier for you to grow your business or brand online. Easily send beautiful emails, start your own blog, get booked online, open your own online store and more. </p>
						<a href="#"><img src="<?php echo base_url('assets/foto/getStarted.jpg')?>" style="cursor: pointer"/></a>
					</div>
					<br>
					<br>
					<div class="row">
						<h1> What Makes Our Website Builder the Best Choice for You? </h1>
						<p> It's simple. With Wix, you get the freedom to create a free website that looks exactly the way you want. It doesn't matter how experienced you are. Prefer to have a website built for you? Try Wix ADI. Need advanced code capabilities? You've got that too. With Wix, you get the whole package, including a website builder, reliable web hosting, top security, and the best SEO for your website. And that's not all, our dedicated Support Team is always here for you. </p>
						<br>
						<p> Over 125 million people worldwide choose Wix to create a website and manage their business online. Still not convinced? Try our free website builder yourself. </p>
						<br>
						<a href="<?php echo base_url('website/login')?>"><img src="<?php echo base_url('assets/foto/getStarted.jpg')?>" style="cursor: pointer"/></a>
					</div>
				</div>
				<div class="col">
					<a href="<?php echo base_url('website/login')?>"><img src="<?php echo base_url('assets/foto/people.jpg')?>"/></a>
				</div>
			</div>

			<!-- Box Bagian bawah -->
			<div class="boxBagianBawah">
				<div class="row">
					<div class="col-sm">
						<h1 style="color:white;font-size:800%;">
							How to <br> 
							Create a <br> 
							Free <br> 
							Website
						</h1>
					</div>
					<div class="col-sm">
						<h5 style="color:white;padding-top:10%;">
							Create a website with Wix’s free website builder in just 5 easy steps:
						</h5>
						<div class="boxPutihKecil"></div>
						<ol>
						  <br>
						  <li>Sign up for a free Wix account. Choose what kind of website you want to create.</li>
						  <br>
						  <li>Answer a few simple questions to get a site created for you with Wix ADI. Or you can start by choosing a designer-made template you like.</li>
						  <br>
						  <li>Customize anything on your site. You can add videos, images, text & more.</li>
						  <br>
						  <li>When you’re ready, publish your website to easily share it with the world.</li>
						  <br>
						  <li>Add more as you grow, like your own online store, booking system and more.</li>
						</ol>  
					</div>
				</div>
			</div>

			<!-- Wix diatas footer -->
			<div class="text-center" style="margin: 5% 0 5% 0">
				<a href="#"><img src="<?php echo base_url('assets/foto/wixAtasFooter.png')?>"/></a>
			</div>

			<hr>

			<!-- Footer -->
			<footer>
				<div class="container">
					<div class="row" style="margin:top:5%">
						<!-- Product -->`
						<div class="col">
							<br><br>
							<h5> Product </h5>
							<br><br>
							<p> Templates </p>
							<p> Explore </p>
							<p> Features </p>
							<p> Website Builder </p>
							<p> Corvid </p>
							<p> Wix Playground </p>
							<p> My Sites </p>
							<p> Premium Plans </p>
							<p> Wix SEO </p>
							<p> Logo Maker </p>
							<p> Start a Blog </p>
							<p> Online Store </p>
							<p> Wix Bookings </p>
							<p> App Market </p>
							<p> Domains </p>
							<p> Mailboxes </p>
							<p> Web Hosting  </p>
							<p> Wix Answers </p>
							<p> Developers </p>
							<p> Enterprise </p>
							<p> Email Marketing </p>
						</div>
						<!-- Company -->
						<div class="col">
							<br><br>
							<h5> COMPANY </h5>
							<br><br>
							<p> About Wix </p>
							<p> Press Room </p>
							<p> Investor Relations </p>
							<p> Wix Jobs </p>
							<p> Design Assets </p>
							<p> Terms of Use </p>
							<p> App Market Terms </p>
							<p> Privacy Policy </p>
							<p> Abuse </p>
							<p> Affiliates </p>
							<p> Updates & Releases </p>
							<p> Contact Us </p>
							<p> Patent Notice </p>
							<p> Sitemap </p>
						</div>
						<!-- Community -->
						<div class="col">
							<br><br>
							<h5> COMMUNITY </h5>
							<br><br>
							<p> Wix Blog </p>
							<p> Wix Arena </p>
							<p> Facebook </p>
							<p> Twitter </p>
							<p> Google+ </p>
							<p> Pinterest </p>
							<p> Youtube </p>
							<p> Linkedin </p>
							<p> Instagram </p>
							<p> Student Website </p>
						</div>
						<!-- Support -->
						<div class="col">
							<br><br>
							<h5> SUPPORT </h5>
							<br><br>
							<p> Support Center </p>
						</div>
						<!-- WIX.com -->
						<div class="col">
							<br><br>
							<a href="#"><img src="<?php echo base_url('assets/foto/wix.jpg')?>"/></a>
							<br><br>
							<p> Wix.com is a leading cloud-based development platform with millions of users worldwide. We make it easy for everyone to create a beautiful, professional web presence. </p>
							<br>
							<p> Promote your business, showcase your art, set up an online shop or just test out new ideas. The Wix website builder has everything you need to create a fully personalized, high-quality free website. © 2006-2019 Wix.com, Inc </p>
							<br>
							<div class="inline">
								<a href="#"><img src="<?php echo base_url('assets/foto/fb.jpg')?>"/></a>
								<a href="#"><img src="<?php echo base_url('assets/foto/twitter.jpg')?>"/></a>
								<a href="#"><img src="<?php echo base_url('assets/foto/g.jpg')?>"/></a>
								<a href="#"><img src="<?php echo base_url('assets/foto/youtube.jpg')?>"/></a>
								<a href="#"><img src="<?php echo base_url('assets/foto/Pinterest.jpg')?>"/></a>
								<a href="#"><img src="<?php echo base_url('assets/foto/ig.jpg')?>"/></a>
								<a href="#"><img src="<?php echo base_url('assets/foto/linkedin.jpg')?>" style="width:10%"/></a>
							</div>
						</div>
					</div>
				</div>
			</footer>



		</div>

			
			<!-- Script -->;
			<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
