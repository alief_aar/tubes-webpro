<?php
defined('BASEPATH') OR exit('No script allowed here');
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login | Wix</title>
		<link rel="icon" type="image/png" href="<?php echo base_url('assets/foto/title.png');?>" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

		<link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet"> 
		
		<!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		
		<style>
			html, body{
				background-color : white;
				font-family : Tw Cen MT;
			}
			.img1{
				margin-top : 10.5%;
				margin-left : -13%;
				width : 15%;
			}
			.img2{
				margin-top : 10%;
				margin-left : 100%;
				width : 15%;
			}
			.btn {
				  background-color: transparent;
				  border: none;
				  color: #40A1EC;
				  padding: 12px 16px;
				  font-size: 20px;
				  cursor: pointer;
			}

			/* Darker background on mouse-over */
			.btn:hover {
				  background-color: #40A1EC;
				  color: white;
			}
			.crossD{
				margin-top : 10%;
				margin-left : 100%;
				color:grey;
				font-size: 40px;
			}
			.crossD:hover{
				color: black;
			}
			.login{
			  max-width:360px;
			  margin-left: 200px;
			  border: none;
			}
			.fontCardContent{
				font-size: 50px;
			}
			form {
				margin-left:40%;
				margin-top: 10%;
				border-radius: 5px;
				background-color: white;
				width: 8%;
				height: 50%;
				position: inline-block;
				align-items: center;
			}
			form input[type="text"],form input[type="password"]{
				border: none;
				margin-right: 30px;
				margin-left: 30px;
				margin-top: 15px;
				width: 250px;
				height: 10%;
				border-bottom: 3px solid #ddd;
				outline: none;
				font-size: 16px;
			}
			.buttonLogin{
				margin: 50px 80px;
				text-align: center;
				text-decoration: none;
				font-size: 20px;
				background-color: white;
				color: #40A1EC;
				border-color: #40A1EC;
				cursor: pointer;
				padding: 5px 50px;
				border-radius:20px;
			}
			.footerr{
				text-align: center;
				margin-top : -5% ;
			}
			button:hover{
				background-color: #40A1EC;
				color: white;
			}
			form input[type="text"]:hover,form input[type="password"]:hover{
				border-color: #000000;
			}
			form input[type="text"]:focus, form input[type="password"]:focus{
				border-color: #40A1EC;  
			}
			form input[type="checkbox"]:focus{
				background-color: #40A1EC;
			}
			hr{ 
				border: none;
				border-left: 1px solid hsla(200, 10%, 50%,100);
				height: 50vh;
				width: 1px;       
			}
			.buttonFb{
				
				background-color: #40A1EC;
			}
			.buttonGoogle{
				margin-top: 3%;
				background-color: #F40E0E;
			}
			.btn-facebook {
			  margin-top: 25%;
			  background: #3B5998;
			  border-radius: 0;
			  color: #fff;
			  border-width: 1px;
			  border-style: solid;
			  border-color: #263961; 
			  padding-top: 5px;
			}
			.btn-facebook:link, .btn-facebook:visited {
			  color: #fff;
			}
			.btn-facebook:active, .btn-facebook:hover {
			  background: #263961;
			  color: #fff;
			}
			.btn-google {
			  margin-top: 3%;
			  background: #F40E0E;
			  border-radius: 0;
			  color: #fff;
			  border-width: 1px;
			  border-style: solid;
			  border-color: #F40E0E;
			  padding-top: 5px;
			}
			.btn-google:link, .btn-google:visited {
			  color: #fff;
			}
			.btn-google:active, .btn-google:hover {
			  background: #F40E0E;
			  color: #fff;
			}
		</style>
	</head>
	<body>
		<div class="container">
		  <div class="row">
			<div class="col">
			  <img class="img1" src="<?php echo base_url('assets/foto/judul.png');?>"/>
			</div>
			<div class="col">
			   <a href="<?php echo base_url('website/index')?>">
				  <span class="glyphicon glyphicon-remove crossD"></span>
			   </a>
			</div>
		  </div>

		  <div class="row">
				<div class="col">
				  <p style="font-size:50px;margin-top:-4%;text-align:center"> Login </p>
				  <p style="font-size:20px;text-align:center">New to Wix? <a href="<?php echo base_url('website/register')?>" style="font-color:blue;text-decoration:none;"/> Sign Up </a> </p>
				</div>
		  </div>
			
		  <div class="row">
			  <div class="col">
			  <form action="<?php echo base_url('website/auth')?>" method="post">
					<input class="input-field" type="text" name="username" placeholder="Username">
					<br>
					<input type="password" name="password" placeholder="Password">
					<button class="buttonLogin" type="submit">Login</button>
			  </form>	
			</div>
			<div class="col">
				<hr>
			</div>
			<div class="col">
				<a href="#" title="Facebook" class="btn btn-facebook"><i class="fa fa-facebook fa-fw"></i> Connect with Facebook</a>
				<a href="#" title="Google" class="btn btn-google"><i class="fa fa-google fa-fw"></i>Connect with Google</a>
			</div>
		  </div>
			  
		<p class="footerr">
			*By logging in, you agree to our <a href="#" style="font-color:blue;text-decoration:none;">Terms of Use </a> and to <br>receive Wix emails & updates and acknowledge that you <br>read our <a href="#" style="font-color:blue;text-decoration:none;"> Privacy Policy. </a>
		</p>
        
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/js/alpha.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://ajax.googleapis.	com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		
        </div>
    </body>
</html>
