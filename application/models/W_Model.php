<?php

class W_Model extends CI_model
{
	private $_tableWebsite = "tb_website";
	private $_tableOrder = "tb_order";
	private $_tableUser = "tb_user";

	public function getAllWebsite()
	{
		//use query builder to get data table "tb_website"
		$query = $this->db->get('tb_website');
		return $query->result_array();
	}

	public function getAllOrder()
	{
		//use query builder to get data table "tb_order"
		$query = $this->db->get('tb_order');
		return $query->result_array();
	}

	public function getAllUser()
	{
		//use query builder to get data table "tb_user"
		$query = $this->db->get('tb_user');
		if ($query->result_array() != NULL){
			$this->load->view();
		}
	}

	public function tambahDataUser($name, $password, $email, $username, $roles){
		return $this->db->query("INSERT INTO `tb_user` (`name`, `password`, `email`,`username`,`roles`) VALUES ('$name', '$password', '$email','$username','$roles')");
	}

	public function tambahDataWebsite($name, $category, $price)
	{
		/*
		$data = [
			"name" => $this->input->post('name', true),
			"category" => $this->input->post('category', true),
			"price" => $this->input->post('price', true),
		];
		*/
		//use query builder to insert $data to table "tb_website"
		//$this->db->insert($this->_tableWebsite, $data);
		return $this->db->query("INSERT INTO `tb_website` (`name`, `category`, `price`) VALUES ('$name', '$category', '$price')");
	}

	public function tambahDataOrder($name, $email, $username, $subscription, $payment, $price)
	{
		/*
		$data = [
			"name" => $this->input->post('name', true),
			"category" => $this->input->post('category', true),
			"price" => $this->input->post('price', true),
		];
		*/
		//use query builder to insert $data to table "tb_website"
		//$this->db->insert($this->_tableWebsite, $data);
		return $this->db->query("INSERT INTO `tb_order` (`name`, `email`, `username`, `subscriptionPlan`, `paymentMethod`, `price`) VALUES ('$name', '$email', '$username','$subscription','$payment','$payment')");
	}

	public function checkUser($username, $password, $roles){
		$this->db->where('username', $username);
        $this->db->where('password', $password);
        return $this->db->get('tb_user')->row_array();
	}

	public function tambahData()
	{
		$data = [
			"name" => $this->input->post('name', true),
			"email" => $this->input->post('email', true),
			"username" => $this->input->post('username', true),
			"subscription" => $this->input->post('subscription', true),
			"payment" => $this->input->post('payment', true),
		];

		//use query builder to insert $data to table "tb_website"
		$this->db->insert($this->_tableWebsite, $data);	
	}

	public function hapusDataWebsite($id)
	{
		//use query builder to delete data based on id
		
		//dicoba kalo ga pake return 
		//print_r($id);
		//$this->db->where("id", $id);  
        //$query = $this->db->delete("tb_website");
		return $this->db->query("DELETE FROM `tb_website` WHERE `id_website`='$id'");
	}

	public function hapusDataOrder($id_order)
	{
		//use query builder to delete data based on id
		$this->db->where("id_order", $id_order);  
        $query = $this->db->delete("tb_order");
		return $query;
	}

	public function getWebsiteById($id)
	{
		//get data mahasiswa based on id 
		$this->db->where("id", $id);  
        $query = $this->db->get("tb_website");  
        return $query;
	}

	public function getOrderByIdOrder($id_order)
	{
		//get data mahasiswa based on id 
		$this->db->where("id_order", $id_order);  
        $query = $this->db->get("tb_order");  
        return $query;
	}

	public function ubahDataWebsite()
	{
		$data = [
			"name" => $this->input->post('name', true),
			"category" => $this->input->post('category', true),
			"price" => $this->input->post('price', true),
		];
		//use query builder class to update data mahasiswa based on id
		/*
			$this->db->where('id', $this->input->post('id'));
			$this->db->update('mahasiswa', $data);

			mahasiswa : nama_tabel;
		*/
		$this->db->set($data);
		$this->db->where('id', $_POST['id']);
		$this->db->update($this->_tableWebsite);
	}

	public function ubahDataOrder()
	{
		$data = [
			"name" => $this->input->post('name', true),
			"email" => $this->input->post('email', true),
			"username" => $this->input->post('username', true),
			"subscription" => $this->input->post('subscription', true),
			"payment" => $this->input->post('payment', true),
		];
		//use query builder class to update data mahasiswa based on id
		
		$this->db->set($data);
		$this->db->where('id_order', $_POST['id_order']);
		$this->db->update($this->_tableWebsite);
	}

	public function cariDataOrder()
	{
		$keyword = $this->input->post('keyword', true);
		//use query builder class to search data order based on keyword "name","email","username","subscription" or "payment"
		$this->db->select("*");
		$this->db->from("tb_order");
		$this->db->like('name', $keyword);
		$this->db->like('email', $keyword);
		$this->db->like('username', $keyword);
		$this->db->like('subscription',$keyword);
		$this->db->like('payment', $keyword);

		/* 
			Kalo ga pake from 
			return $this->db->get('Mahasiswa')->result_array();
		*/

		//return data order that has been searched
		return $this->db->get()->result_array();
	}

	public function cariDataWebsite()
	{
		$keyword = $this->input->post('keyword', true);
		//use query builder class to search data website based on keyword "name", or "category", or "price"
		$this->db->select("*");
		$this->db->from("tb_website");
		$this->db->like('name', $keyword);
		$this->db->like('category', $keyword);
		$this->db->like('price', $keyword);

		//return data website that has been searched
		return $this->db->get()->result_array();
	}

	public function ubahOrder($id_order)
	{
		$data['tb_order'] = $this->W_Model->getOrderByIdOrder($id_order);

		//"name","email","username","subscription" or "payment"

		$this->form_validation->set_rules('name','name','required');
		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('subscription','subscription','required');
		$this->form_validation->set_rules('payment','payment','required');

		if($this->form_validation->run() != false){
			$this->W_Model->ubahDataOrder();
			redirect('Website');
		}
		else{
			$this->load->view('crud/crud_pemesanan',$data);
		}
	}

	public function ubahWebsite($id)
	{
		$data['tb_website'] = $this->W_Model->getWebsiteById($id);

		//"name","email","username","subscription" or "payment"

		$this->form_validation->set_rules('name','name','required');
		$this->form_validation->set_rules('category','category','required');
		$this->form_validation->set_rules('price','price','required');

		if($this->form_validation->run() != false){
			$this->W_Model->ubahDataOrder();
			redirect('Website');
		}
		else{
			$this->load->view('crud/crud_website',$data);
		}
	}
	
	public function countWebsite()
	{
		return $this->db->query("SELECT * FROM `tb_website`")->num_rows();
	}

	public function countOrder()
	{
		return $this->db->query("SELECT * FROM `tb_order`")->num_rows();
	}

	public function validate($username,$password){
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$result = $this->db->get('tb_user',1);
		return $result;
	}
	public function getUserByUsername($username){
		return $this->db->query("SELECT * FROM `tb_user` WHERE `username`='$username'")->result();
	}
}

