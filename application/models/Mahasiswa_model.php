<?php
 // Alief Aditya Rachman | 1301174055
class Mahasiswa_model extends CI_model
{
	private $_table = "mahasiswa";
	public function getAllMahasiswa()
	{
		//use query builder to get data table "mahasiswa"
		$query = $this->db->get('mahasiswa');
		return $query->result_array();
	}

	public function tambahDataMahasiswa()
	{
		$data = [
			"nama" => $this->input->post('nama', true),
			"nim" => $this->input->post('nim', true),
			"email" => $this->input->post('email', true),
			"jurusan" => $this->input->post('jurusan', true),
		];

		//use query builder to insert $data to table "mahasiswa"
		$this->db->insert($this->_table, $data);	
	}

	public function hapusDataMahasiswa($id)
	{
		//use query builder to delete data based on id
		$this->db->where("id", $id);  
        $query = $this->db->delete("mahasiswa");
		//$query = $this->db->delete('mahasiswa', array('id' => $id));
		return $query;
	}

	public function getMahasiswaById($id)
	{
		//get data mahasiswa based on id 
		$this->db->where("id", $id);  
        $query = $this->db->get("mahasiswa");  
        return $query;
		//return $this->db->get_where('mahasiswa', array('id' => $id));
		//$query = $this->db->get_where('mahasiswa', array('id' => $id), $limit, $offset);
	}

	public function ubahDataMahasiswa()
	{
		$data = [
			"nama" => $this->input->post('nama', true),
			"nim" => $this->input->post('nim', true),
			"email" => $this->input->post('email', true),
			"jurusan" => $this->input->post('jurusan', true),
		];
		//use query builder class to update data mahasiswa based on id
		//$this->db->update($this->mahasiswa, $this, array('nama' => $post['nama']));
		//$this->db->update($this->mahasiswa, $this, array('nim' => $post['nim']));
		//$this->db->update($this->mahasiswa, $this, array('email' => $post['email']));
		//$this->db->update($this->mahasiswa, $this, array('jurusan' => $post['jurusan']));
		//return $this->db->replace('mahasiswa', $data);
		//$this->db->where('nama',  $this->input->post('nama', true));
		//return $this->db->update('mahasiswa', $data);
		//return $this->db->update($this->_table, $data, array('nama' => $data['nama']));
		$this->db->set($data);
		$this->db->where('id', $_POST['id']);
		$this->db->update($this->_table);
	}

	public function cariDataMahasiswa()
	{
		$keyword = $this->input->post('keyword', true);
		//use query builder class to search data mahasiswa based on keyword "nama" or "jurusan" or "nim" or "email"
		$this->db->select('*');
		$this->db->from('mahasiswa');
		$this->db->like('nama',$keyword);
		$this->db->or_like('jurusan',$keyword);
		$this->db->or_like('nim',$keyword);
		$this->db->or_like('email',$keyword);

		//return data mahasiswa that has been searched
		return $this->db->get()->result_array();
	}
	 public function ubah($id)
	 {
	  $data['judul'] = 'Form Ubah Data Mahasiswa';

	  $data['mahasiswa'] = $this->Mahasiswa_model->getMahasiswaById($id);
	  $data['jurusan'] = ['Teknik Informatika', 'Teknik Industri', 'Teknik Elektro', 'DKV', 'MBTI'];

	  //from library form_validation, set rules for nama, nim, email = required
	  $this->form_validation->set_rules('nama','Nama','required');
	  $this->form_validation->set_rules('nim','Nim','required');
	  $this->form_validation->set_rules('email','Email','required');

	  //conditon in form_validation, if user input form = false, then load page "ubah" again
	  if($this->form_validation->run() == false){
	   $this->load->view('templates/header',$data);
	   $this->load->view('mahasiswa/ubah',$data);
	   $this->load->view('templates/footer');
	  }else{
	   //else, when successed {
	   //call method "ubahDataMahasiswa" in Mahasiswa_model
	   //use flashdata to to show alert "data changed successfully"
	   //back to controller mahasiswa }
	   $this->Mahasiswa_model->ubahDataMahasiswa();
	   $this->session->set_flashdata('flash','data changed successfully');
	   redirect('Mahasiswa');
	  }
	}
}

