<?php

class Website extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//load model "W_Model"
		$this->load->database();
		$this->load->model('W_Model');
		//load library form validation
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function index()
	{
		$this->load->view('home/index');
	}

	public function crud_pemesanan(){
		$data['judul'] = 'Daftar Order';
		$data['order'] = $this->W_Model->getAllOrder();
		$data['jumlah'] = $this->W_Model->countOrder();
		//if ($this->input->post('keyword')) {
		//	$data['mahasiswa'] = $this->W_Model->getAllWebsite();
		//}
		$this->load->view('crud/crud_pemesanan',$data);
	}

	public function crud_website(){
		$data['judul'] = 'Daftar Website';
		$data['website'] = $this->W_Model->getAllWebsite();
		$data['jumlah'] = $this->W_Model->countWebsite();
		//if ($this->input->post('keyword')) {
		//	$data['mahasiswa'] = $this->W_Model->getAllWebsite();
		//}
		$this->load->view('crud/crud_website',$data);
	}

	public function template()
	{
		$this->load->view('home/template.php');
	}

	public function register(){
		$this->load->view('awal/register.php');
	}
	
	public function payment($paket)
	{
		//print_r($paket);
		if($paket=='vip'){
			$data['paket']=24.5;
		}
		else if($paket=='ecommerce'){
			$data['paket']=8.25;
		}
		else if($paket=='unlimited'){
			$data['paket']=6.25;
		}
		else if($paket=='combo'){
			$data['paket']=6.25;
		}
		else if($paket=='connect'){
			$data['paket']=4.5;
		}
		
		$this->load->view('upgrade/payment.php',$data);
	}

	public function login()
	{
		$this->load->view('awal/login.php');
	}

	public function upgrade($username)
	{
		$productID =  $this->uri->segment(4);
		if($productID=='vip'){
			$data['paket']=24.5;
			$this->load->view('upgrade/payment.php',$data);
		}else if($productID=='ecommerce'){
			$data['paket']=8.25;
			$this->load->view('upgrade/payment.php',$data);
		}else if($productID=='unlimited'){
			$data['paket']=6.25;
			$this->load->view('upgrade/payment.php',$data);
		}else if($productID=='combo'){
			$data['paket']=6.25;
			$this->load->view('upgrade/payment.php',$data);
		}else if($productID=='connect'){
			$data['paket']=4.5;
			$this->load->view('upgrade/payment.php',$data);
		}
		$data['user'] = $this->W_Model->getUserByUsername($username);
		$this->load->view('upgrade/upgrade.php',$data);
	}

	public function tambahUser()
	{

		//from library form_validation, set rules for "name","email","username","subscription" or "payment" = required
		
		$this->form_validation->set_rules('name','name','required');
		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','password','required');
		$this->form_validation->set_rules('roles','roles','required');

		//conditon in form_validation, if user input form = false, then load page "crud_website" again
		if($this->form_validation->run() == false){
			$this->load->view('home/register', $data);
		}
		else{
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$roles = $this->input->post('roles');
			$this->W_Model->tambahDataUser($name, $password, $email, $username, $roles);
			//set alert for succession 
			$this->session->set_flashdata('flash','added success');
			//back to controller Website
			redirect(base_url()."Website/index");
		}
	}

	public function tambahOrder()
	{

		//from library form_validation, set rules for "name","email","username","subscription" or "payment" = required
		
		$this->form_validation->set_rules('name','name','required');
		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('subscription','subscription','required');
		$this->form_validation->set_rules('payment','payment','required');
		$this->form_validation->set_rules('price','price','required');
		//conditon in form_validation, if user input form = false, then load page "crud_pemesanan" again
		if($this->form_validation->run() == false){
			$this->load->view('crud/crud_pemesanan', $data);
		}
		else{
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$username = $this->input->post('username');
			$subscription = $this->input->post('subscription');
			$payment = $this->input->post('payment');
			$price = $this->input->post('price');
			$this->W_model->tambahDataOrder($name, $email, $username, $subscription, $payment, $price);
			//set alert succession 
			$this->session->set_flashdata('flash','added success');
			//back to controller Website
			redirect(base_url()."Website/crud_pemesanan");
		}
	}

	public function tambahWebsite()
	{

		//from library form_validation, set rules for "name","email","username","subscription" or "payment" = required
		
		$this->form_validation->set_rules('name','name','required');
		$this->form_validation->set_rules('category','category','required');
		$this->form_validation->set_rules('price','price','required');

		//conditon in form_validation, if user input form = false, then load page "crud_website" again
		if($this->form_validation->run() == false){
			$this->load->view('crud/crud_website', $data);
		}
		else{
			$name = $this->input->post('name');
			$category = $this->input->post('category');
			$price = $this->input->post('price');
			$this->W_Model->tambahDataWebsite($name, $category, $price);
			//set alert for succession 
			$this->session->set_flashdata('flash','added success');
			//back to controller Website
			redirect(base_url()."Website/crud_website");
		}
	}

	public function auth(){
		$username    = $this->input->post('username',TRUE);
		$password    = $this->input->post('password',TRUE);
		$validate    = $this->W_Model->validate($username,$password);
		if($validate->num_rows() > 0){
			$data  = $validate->row_array();
			$username  = $data['username'];
			$email = $data['email'];
			$roles = $data['roles'];
			$sesdata = array(
				'username'  => $username,
				'email'     => $email,
				'roles'     => $roles,
				'logged_in' => TRUE
			);
			$this->session->set_userdata($sesdata);
			// access login for admin
			if($roles === 'admin'){
				redirect(base_url()."Website/crud_pemesanan");
 
			// access login for user
			}elseif($roles === 'user'){
				redirect(base_url()."Website/upgrade/".$username);
			}
		}else{
			echo $this->session->set_flashdata('msg','Username or Password is Wrong');
			redirect(base_url()."Website/login");
		}
		
	}

	public function hapusOrder($id_order)
	{
		//call method hapusDataOrder with parameter id from W_Model
		$this->W_Model->hapusOrder($id_order);

		//use flashdata to show alert "dihapus"
		$this->session->set_flashdata('flash','deleted success');
		
		//back to controller mahasiswa
		redirect(base_url()."Website/index");
	}

	public function hapusWebsite($id)
	{
		//call method hapusDataOrder with parameter id from W_Model
		$this->W_Model->hapusWebsite($id);

		//use flashdata to show alert "dihapus"
		$this->session->set_flashdata('flash','deleted success');
		
		//back to controller mahasiswa
		redirect(base_url()."Website/index");
	}

	public function ubahOrder($id_order)
	{
		  $data['tb_order'] = $this->W_Model->getOrderByIdOrder($id_order);
		  $data['subscription'] = ['Monthly Subscription','Yearly Subscription','2 Year Subscription'];
		  $data['payment'] = ['Visa', 'Mastercard', 'American Express', 'Discover'];

		  //from library form_validation, set rules for name, email, username, subscription, payment = required
		  $this->form_validation->set_rules('name','name','required');
		  $this->form_validation->set_rules('email','email','required');
		  $this->form_validation->set_rules('username','username','required');
		  $this->form_validation->set_rules('subscription','subscription','required');
		  $this->form_validation->set_rules('payment','payment','required');

		  //conditon in form_validation, if user input form = false, then load page "ubah" again
		  if($this->form_validation->run() == false){
			   $this->load->view('crud/crud_pemesanan',$data);
		  }
		  else{
			   //else, when successed {
			   //call method "ubahDataMahasiswa" in Mahasiswa_model
			   $this->W_Model->ubahOrder();
			   //use flashdata to to show alert "data changed successfully"
			   $this->session->set_flashdata('flash','data changed successfully');
			   //back to controller mahasiswa }
			   redirect('Website');
		  }
	}

	public function ubahWebsite($id)
	{
		  $data['tb_website'] = $this->W_Model->getWebsiteById($id);
		  $data['category'] = ['Business','Automotive','Motivational','Personal'];
		  $data['price'] = ['Free', 'IDR 1.000.000,00', 'IDR 2.000.000,00', 'IDR 3.000.000,00'];

		  //from library form_validation, set rules for name, email, username, subscription, payment = required
		  $this->form_validation->set_rules('name','name','required');
		  $this->form_validation->set_rules('category','category','required');
		  $this->form_validation->set_rules('price','price','required');

		  //conditon in form_validation, if user input form = false, then load page "ubah" again
		  if($this->form_validation->run() == false){
			   $this->load->view('crud/crud_website',$data);
		  }
		  else{
			   //else, when successed {
			   //call method "ubahDataMahasiswa" in Mahasiswa_model
			   $this->W_Model->ubahWebsite();
			   //use flashdata to to show alert "data changed successfully"
			   $this->session->set_flashdata('flash','data changed successfully');
			   //back to controller mahasiswa }
			   redirect('Website');
		  }
	}
	public function delete($id)
	{
		//print_r($id);
		$this->W_Model->hapusDataWebsite($id);
		redirect('Website');
	}

	public function deleteOrder($id_order)
	{
		//print_r($id);
		$this->W_Model->hapusDataOrder($id_order);
		redirect('Website');
	}

	public function admin(){
		if($this->session->userdata('roles')==='admin'){
          $this->load->view('crud/crud_pemesanan');
      }
	}

	public function user(){
		if($this->session->userdata('roles')==='user'){
          $this->load->view('upgrade/upgrade');
      }
	}

}

